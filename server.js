var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var requestjson = require('request-json');

var urlRaizMLab  = "https://api.mlab.com/api/1/databases/dmorales/collections";

var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var clienteMLabRaiz;

var urlClientes = "https://api.mlab.com/api/1/databases/dmorales/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urClienteSinApikey = "https://api.mlab.com/api/1/databases/dmorales/collections/Clientes/";

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var movimientosJSON = require('./movimientosv2.json');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get ('/', function(req, res){
  res.sendFile(path.join(__dirname, 'index.html'));
})

app.get ('/Clientes/:idcliente', function(req, res){
  res.send('Aquí tienes al cliente número: '+ req.params.idcliente);
})

app.get ('/v1/movimientos', function(req, res){
  res.sendfile('movimientosv1.json');
})

app.get ('/v2/movimientos', function(req, res){
  res.json(movimientosJSON);
})

app.get ('/v2/movimientos/:indice', function(req, res){
  console.log(req.params.indice)
  res.send(movimientosJSON[req.params.indice]);
})

app.get ('/v3/movimientosquery', function(req, res){
  console.log(req.query)
  res.send('Recibido');
})

app.post ('/', function(req, res){
  res.send('Hemos recibido su petición post');
})

app.post('/v3/movimientos', function(req, res){
  var nuevo = req.body;
  nuevo.id = movimientosJSON.length + 1;
  movimientosJSON.push(nuevo);
  res.send('Movimiento dado de alta');
})

app.put ('/', function(req, res){
  res.send('Hemos recibido su petición cambiada');
})

app.delete ('/', function(req, res){
  res.send('Hemos recibido su petición delete');
})

app.get ('/Clientes', function(req, res){
  var clienteMLab = requestjson.createClient(urlClientes);
  clienteMLab.get('', function(err, resM, body) {
    if(err){
      console.log(body);
    } else{
      res.send(body);
    }
  })
})

app.post('/login', function(req, res){
  res.set("Access-Control-Allow-Headers", "Content-Type");
  var _id = req.body.email;
  var password = encriptar(req.body.email,req.body.password);
  var query = 'q={"_id":"'+_id+'","password":"'+password+'"}';

  clienteMLabRaiz = requestjson.createClient(urlRaizMLab + "/Usuarios?" + apiKey + "&" + query);
  console.log(urlRaizMLab + "/Usuarios?" + apiKey + "&" + query);

  clienteMLabRaiz.get('', function(err, resM, body){
    if(!err){
      if(body.length == 1){ //Login ok
        res.status(200).send('Usuario logeado');
      } else{
        res.status(404).send('Usuario no encontrado, registrese');
      }
    }
  })
})

app.get ('/MovimientosIdCliente/:idcliente', function(req, res){
  var idCliente = req.params.idcliente;
  var filtro = 'f={movimientos: 1, _id: 0}';
  var query = 'q={"_id": "'+idCliente+'"}';
  var sort = 's={movimientos.fecha: 1}'
  var clienteMLab = requestjson.createClient(urlClientes + "&" + filtro + "&" + query + "&" + sort);
  console.log("Peticion: " + urlClientes + "&" + filtro + "&" + query + "&" + sort);
  clienteMLab.get('', function(err, resM, body) {
    if(err){
      console.log(body);
    } else{
      res.send(body);
    }
  })
})

app.put('/AltaMovimiento', function(req, res){
  res.set("Access-Control-Allow-Headers", "Content-Type");

  var idCliente = req.body.idcliente;
  var clienteMLab = requestjson.createClient(urClienteSinApikey + idCliente + "?" + apiKey);
  console.log("Peticion: " + urClienteSinApikey + idCliente + "?" + apiKey);

  var data = {
    $addToSet: {
        movimientos: {
          "fecha": formatoFecha(req.body.fecha),
          "importe": parseFloat(req.body.importe),
          "categoria": req.body.categoria
        }
      }
    }

  console.log(data);

  clienteMLab.put('', data, function(err, resM, body) {
    res.status(200).send('Movimiento registrado correctamente');
  })
})

app.post('/AltaUsuario', function(req, res){
  res.set("Access-Control-Allow-Headers", "Content-Type");
  clienteMLabRaiz = requestjson.createClient(urlRaizMLab + "/Usuarios?" + apiKey);
  console.log(urlRaizMLab + "/Usuarios?" + apiKey);

  var data = {
    _id: req.body.email,
    password: encriptar(req.body.email,req.body.password)
  };

  clienteMLabRaiz.post('', data, function(err, resM, body){
        res.status(200).send('Usuario creado correctamente');
  })
})

app.post('/AltaCliente', function(req, res){
  res.set("Access-Control-Allow-Headers", "Content-Type");
  clienteMLabRaiz = requestjson.createClient(urlClientes);
  console.log("Peticion: " + urlClientes);

  var data = {
    _id: req.body.idcliente,
    nombre: req.body.nombre,
    apellido: req.body.apellido,
    movimientos: []
  };

clienteMLabRaiz.post('', data, function(err, resM, body){
      res.status(200).send('Cliente creado correctamente');
    })
})

app.delete('/EliminarUsuario', function(req, res){
  res.set("Access-Control-Allow-Headers", "Content-Type");
  var _id = req.body.email;
  clienteMLabRaiz = requestjson.createClient(urlRaizMLab + "/Usuarios/" + _id + "?" + apiKey);
  console.log(urlRaizMLab + "/Usuarios/" + _id + "?" + apiKey);

  clienteMLabRaiz.del('', function(err, resM, body){
        res.status(200).send('Usuario eliminado correctamente');
  })
})

app.put('/ActualizarPassword', function(req, res){
  res.set("Access-Control-Allow-Headers", "Content-Type");

  var _id = req.body.email;
  var clienteMLab = requestjson.createClient(urlRaizMLab + "/Usuarios/" + _id + "?" + apiKey);
  console.log("Peticion: " + urlRaizMLab + "/Usuarios/" + _id + "?" + apiKey);

  var data = {
    $set: {
          "password": encriptar(req.body.email,req.body.password)
      }
    }

  console.log(data);

  clienteMLab.put('', data, function(err, resM, body) {
    res.status(200).send('Se ha actualizado la password del usuario');
  })
})

function formatoFecha(dateStr){
  var partes = dateStr.split("-");
  var nuevaFecha = partes[2]+"/"+partes[1]+"/"+partes[0];
  return nuevaFecha;
}

function encriptar(usuario, password){
  var crypto = require('crypto');
  var hmac = crypto.createHmac('sha512',usuario).update(password).digest('hex');
  return hmac;
}
